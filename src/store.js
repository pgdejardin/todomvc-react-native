import {fromJS} from 'immutable';
import { createStore, compose, applyMiddleware } from 'redux';
import devTools from 'remote-redux-devtools';

import createReducer from './Reducers';

export const configureStore = (initialStore = fromJS({})) => {
  const enhancers = [];

  if (__DEV__) {
    enhancers.push(devTools());
  }

  return createStore(createReducer(), initialStore, compose(...enhancers));
};
