/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

//noinspection JSUnresolvedVariable
import React from 'react';
//noinspection JSUnresolvedVariable
import { AppRegistry } from 'react-native';
import Root from './src/index';

//noinspection JSCheckFunctionSignatures
AppRegistry.registerComponent('MyFirstApp', () =>  Root);
